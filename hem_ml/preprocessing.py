from numpy import argmax
import pandas as pd
import numpy as np
from decimal import Decimal

class CottonPreprocessing:
	'''
	'returns '
	u = unique values 
	df = preprocessed data 
	'''
	def handle_non_numerical_data(df):
		columns=df.columns.values
		u=[]# unique values 
		for column in columns:
			text_digit_vals={}
			def convert_to_int(val):
				return text_digit_vals[val]
			if df[column].dtype!=np.int64  and df[column].dtype!=np.float64:
				#column_contents=df[column].unique()
				unique_elements=df[column].unique()
				x=0
				for unique in unique_elements:
					if unique not in text_digit_vals:
						text_digit_vals[unique]=x
						x+=1
				df[column]=list(map(convert_to_int,df[column]))
				u.append(text_digit_vals)
		return df,u

	# def handle_non_numerical_data_test(df,u):
	# 	columns=df.columns.values
	# 	i=0
	# 	for column in columns:
	# 		print("\n\n")
	# 		# print(column)
	# 		text_digit_vals={}
	# 		def convert_to_int(val):
	# 			# print(val)
	# 			# return text_digit_vals.setdefault(val)
	# 			return text_digit_vals[val]
	# 		if df[column].dtype!=np.int64 and df[column].dtype!=np.float64:
	# 			x=0
	# 			k=u[i]
	# 			for unique in k:
	# 				print("\n\n")
	# 				print(text_digit_vals)
	# 				if unique not in text_digit_vals:
	# 					text_digit_vals[unique]=x
	# 					x+=1

	# 			df[column]=list(map(convert_to_int,df[column]))
	# 		i+=1
	# 		#print(text_digit_vals)
	# 	return df
	def test_dict(df,u):
		p=[]
		for row in range(0,len(df)):
			i=0
			for column in df[row]:
				for key,value in u[i].items():
					if(key==column):
						p.append(value)
				i+=1		
		return p
		

	def preprocessing(h):
		df=pd.read_csv(h)
		df=df.drop(columns=['commodity','state','district'])
		df=pd.DataFrame(df,columns=['market','variety','arrival_date','modal_price'])
		df['arrival_date']=pd.to_datetime(df['arrival_date']).dt.strftime('%Y%m%d').astype(int)
		df['modal_price']=df.modal_price.astype(np.dtype('int64'))
		df,u=CottonPreprocessing.handle_non_numerical_data(df)
		return df,u


