import pandas as pd 
import numpy as np 
from sklearn.preprocessing import MinMaxScaler

filename = './dataset/Cotton_2019.csv';
df = pd.read_csv(filename)
df = df.drop(columns=['state','district','commodity','min_price','max_price'])
df['arrival_date'] = pd.to_datetime(df.arrival_date)
# time_stamp = (df['arrival_date']).copy()
data = df.sort_values(by=['arrival_date']).copy()
print(data.head(),data.tail())
# data = (df.drop(columns=['arrival_date'])).copy()
# print(data.head())
print(data)
exit()
for __id,(_id,row) in enumerate(data.iterrows()):
    (market,variety,
        time_stamp,label) = (row['market'],row['variety'],
                            row['arrival_date'],row['modal_price'])
    print(__id,market,variety,time_stamp,label)
    if __id == 5:exit()