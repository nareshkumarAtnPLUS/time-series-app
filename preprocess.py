import pandas as pd 
import numpy as np 
from sklearn.preprocessing import MinMaxScaler
from hem_ml import CottonPreprocessing
import tensorflow as tf 

filename = './dataset/Cotton_2019.csv';
df = pd.read_csv(filename)
print(len(df))
df = df.drop(columns=['state','district','commodity','min_price','max_price'])

df['arrival_date'] = pd.to_datetime(df.arrival_date)
df = df.sort_values(by=['arrival_date'])
time_stamp = np.array((df['arrival_date']).values.copy())
data = (df.drop(columns=['arrival_date'])).copy()
# print(data.head())
data,u = CottonPreprocessing.handle_non_numerical_data(data)
# df.market,df.variety=data_processed.market,data_processed.variety
print(data.columns)
data = data.values
sc = MinMaxScaler(feature_range = (0,1))
data_set = sc.fit_transform(data)
# time_sc = sc.fit_transform(time_stamp)
# X_train, y_train = data_set[:,:-1],data_set[:,-1:]
# print(X_train.shape,y_train.shape,sep='\n')

X_train,y_train = [],[]
for i in range(60,2035):
    X_train.append(training_set_scaled[i-60:i,0])
    y_train.append(training_set_scaled[i,60])